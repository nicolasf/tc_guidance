# Tropical Cyclone ICU seasonal guidance 

**Repository of python codes producing some of the diagnostics and plots entering into the TC guidance released by NIWA every year in October**

--- 

## Content

### code 

1. **get_all_BOM.py**: get the data from [http://www.bom.gov.au/cyclone/history/tracks/index.shtml](http://www.bom.gov.au/cyclone/history/tracks/index.shtml) and save in ./data/BOM_data_1969-2010.csv

2. **make_categories_BoM.py**: take the BoM data and determine the category of each storm based on the minimum central pressure (if available)

3. **calculate_density.py**: calculate density of TC per season (early, late, full)

4. **plot_TC_density_anomalies.py**: plots the maps of composite mean, anomalies and climatology for density given a list of analog years

5. **plot_TC_tracks_BoM.py**: plot the TC tracks and counts the number of storm in each category given the list of analog years 
