#!/usr/bin/python                                                                                  
# -*- coding: utf-8 -*-
# ==============================================================================
# code calculate_density.py
# Nicolas Fauchereau <Nicolas.Fauchereau@gmail.com>
# ==============================================================================

import os, sys
from datetime import datetime, timedelta
from dateutil import parser
from time import ctime
import numpy as np
from numpy import ma
import pandas as pd
from monthdelta import MonthDelta
from netCDF4 import Dataset
from netcdf import save_3D
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap

dpath = os.path.join(os.environ['HOME'], 'operational/ICU/TC_guidance/data')

### ===========================================================================
### function for distance calculation
def haversine(lon1, lat1, lon2, lat2):
    """ return km
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    Parameters
    ----------
        lon first location,
        lat first location,
        lon second location,
        lat second location

    Returns
    -------
        km: the distance between location 1 and 2 in km
    """
    from math import radians, cos, sin, asin, sqrt
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c 
    return km

### ===========================================================================

tcs_bom = pd.read_csv(os.path.join(dpath,'BOM_data_1969-2010.csv'), header=None,\
                              names=['year','number','name','iso_datetime','lat','lon','central_pressure'], index_col=3, parse_dates=True)

tcs_bom['central_pressure'][tcs_bom['central_pressure'] <= 700] = np.nan

tcs_bom['year'] = tcs_bom.index.year
tcs_bom['month'] = tcs_bom.index.month
tcs_bom['day'] = tcs_bom.index.day

tcs_bom = tcs_bom.ix['1969-1':]

### ===========================================================================
### restrict to 135E 
tcs_bom = tcs_bom[tcs_bom['lon'] >= 135.]

### ===========================================================================q
max_dist = 500. 

res = 5.

latitudes = np.arange(-60., 0. + res, res)[::-1]
longitudes = np.arange(135., 240. + res, res)

### ===========================================================================
"""
TC density per month
"""
datetime_index = [] # index as a datetime object 
grid_storm=[] # grid_storm will be our container of grids 
for y in xrange(1969, 2010): # BoM data stops (includes) the season 2009 - 2010
    for m in xrange(1,13):
        mat = tcs_bom.ix[(tcs_bom['year'] == y) & (tcs_bom['month'] == m),:]
        if len(mat) == 0:
            grid = np.zeros((len(latitudes), len(longitudes)))
        else: 
            grid = np.zeros((len(latitudes), len(longitudes)))
            for ilat, latval in enumerate(latitudes): 
                for ilon, lonval in enumerate(longitudes):
                    dists = np.array([haversine(latval, lonval, lattc, lontc) for lattc,lontc in zip(mat.lat.values, mat.lon.values)])
                    mat['distance'] = dists
                    mat['count'] = (mat['distance'] <= max_dist).astype(np.int32)
                    ### aggregate by storm NUMBER, not storm name 
                    count = mat['count'].groupby(mat['number']).sum().values
                    grid[ilat,ilon] = ((count >= 1).astype(np.int32)).sum()
        grid_storm.append(grid)
        datetime_index.append(datetime(y,m,1))

grid_storm = np.array(grid_storm)

np.savez('../data/BoM_grid_storm_%sx%s_%skm' % tuple(map(int, (res, res, max_dist))), grid_storm=grid_storm, lat=latitudes, lon=longitudes)

origin = datetime(1969,1,1)

from calendar import monthrange

c = 0
ntime = []
for d in datetime_index: 
    ntime.append(c)
    c += monthrange(d.year, d.month)[1]


ncfileout = 'BoM_monthly_count_1969_2010%sx%s_%skm.nc' % tuple(map(int,(res, res, max_dist)))

### ==============================================================================================================
nwrite = Dataset(os.path.join(dpath, ncfileout), 'w', format='NETCDF4')
nwrite.createDimension('time', None) # unlimited dimension
nwrite.createDimension('lat', len(latitudes)) 
nwrite.createDimension('lon', len(longitudes)) 

### create dimension variables 
itime = nwrite.createVariable('time','u4',('time',))
lats = nwrite.createVariable('lat','f4',('lat',)) # f8: 64 bits float,f4: 32 bits floats 
lons = nwrite.createVariable('lon','f4',('lon',)) # ... 

### set up some attributes
itime.units = "days since %s 00:00:00.00" % ( origin.strftime("%Y-%m-%d") )

lats.long_name = "latitude"
lons.long_name = "longitudes"

lats.units = "degrees_north"
lons.units = "degrees_east"

lats.valid_min = -90.
lats.valid_max = 90. 

lons.valid_min = -180.
lons.valid_max = 180.

### TC
tc_count = nwrite.createVariable('count','f4',('time', 'lat','lon',)) 
tc_count.units = 'number'
tc_count.long_name = "Tropical Cyclones counts per month"

### ============================================================================
lats[:] = latitudes
lons[:] = longitudes

for i in xrange(grid_storm.shape[0]): 
    itime[i] = ntime[i]
    tc_count[i,:,:] = grid_storm[i,:,:]

nwrite.close()


### ===========================================================================
"""
now calculate density per season (early, late, full)
"""
grid_storm_df = pd.DataFrame(np.reshape(grid_storm, ((grid_storm.shape[0], grid_storm.shape[1] * grid_storm.shape[2])), order='F'))

grid_storm_df.index = datetime_index

### ===========================================================================
"""
FULL SEASON
"""
datetime_index = [] # index as a datetime object 
full_season=[] # grid_storm will be our container of grids 
number_of_storms =[]
for y in xrange(1969, 2010): 
    mat = tcs_bom.ix['%s-11' % ( y ):'%s-04' % ( y + 1 )]
    number_of_storms.append(len(np.unique(mat.number)))
    if len(mat) == 0:
        grid = np.zeros((len(latitudes), len(longitudes)))
    else: 
        grid = np.zeros((len(latitudes), len(longitudes)))
        for ilat, latval in enumerate(latitudes): 
            for ilon, lonval in enumerate(longitudes):
                dists = np.array([haversine(latval, lonval, lattc, lontc) for lattc,lontc in zip(mat.lat.values, mat.lon.values)])
                mat['distance'] = dists
                mat['count'] = (mat['distance'] <= max_dist).astype(np.int32)
                ### aggregate by storm NUMBER, not storm name 
                count = mat['count'].groupby(mat['number']).sum().values
                grid[ilat,ilon] = ((count >= 1).astype(np.int32)).sum()
    full_season.append(grid)
full_season = np.array(full_season)

save_3D(os.path.join(dpath, 'BoM_tc_count_%sx%s_%skm_1969_2010_full_season.nc' % tuple(map(int, (res, res, max_dist)))), \
        full_season, 'count', latitudes, longitudes, -999.9, np.arange(full_season.shape[0]))

np.savez('../data/BoM_grid_storm_%sx%s_%skm_1969_2012_full_season' % tuple(map(int, (res, res, max_dist))), full_season=full_season, lat=latitudes, lon=longitudes, season='NDJFMA')

### ===========================================================================
"""
EARLY SEASON 
"""
datetime_index = [] # index as a datetime object 
early_season=[] # grid_storm will be our container of grids 
number_of_storms =[]
for y in xrange(1969, 2010): 
    mat = tcs_bom.ix['%s-11' % ( y ):'%s-01' % (y + 1)]
    number_of_storms.append(len(np.unique(mat.number)))
    if len(mat) == 0:
        grid = np.zeros((len(latitudes), len(longitudes)))
    else: 
        grid = np.zeros((len(latitudes), len(longitudes)))
        for ilat, latval in enumerate(latitudes): 
            for ilon, lonval in enumerate(longitudes):
                dists = np.array([haversine(latval, lonval, lattc, lontc) for lattc,lontc in zip(mat.lat.values, mat.lon.values)])
                mat['distance'] = dists
                mat['count'] = (mat['distance'] <= max_dist).astype(np.int32)
                ### aggregate by storm NUMBER, not storm name 
                count = mat['count'].groupby(mat['number']).sum().values
                grid[ilat,ilon] = ((count >= 1).astype(np.int32)).sum()
    early_season.append(grid)
early_season = np.array(early_season)

save_3D(os.path.join(dpath, 'BoM_tc_count_%sx%s_%skm_1969_2010_early_season.nc' % tuple(map(int, (res, res, max_dist)))), \
        early_season, 'count', latitudes, longitudes, -999.9, np.arange(early_season.shape[0]))

np.savez('../data/BoM_grid_storm_%sx%s_%skm_1969_2012_early_season' % tuple(map(int, (res, res, max_dist))), early_season=early_season, lat=latitudes, lon=longitudes, season='NDJ')

### ===========================================================================
"""
LATE SEASON
"""
datetime_index = [] # index as a datetime object 
late_season=[] # grid_storm will be our container of grids 
number_of_storms =[]
for y in xrange(1969, 2010): 
    mat = tcs_bom.ix['%s-02' % ( y ):'%s-04' % ( y )]
    number_of_storms.append(len(np.unique(mat.number)))
    if len(mat) == 0:
        grid = np.zeros((len(latitudes), len(longitudes)))
    else: 
        grid = np.zeros((len(latitudes), len(longitudes)))
        for ilat, latval in enumerate(latitudes): 
            for ilon, lonval in enumerate(longitudes):
                dists = np.array([haversine(latval, lonval, lattc, lontc) for lattc,lontc in zip(mat.lat.values, mat.lon.values)])
                mat['distance'] = dists
                mat['count'] = (mat['distance'] <= max_dist).astype(np.int32)
                ### aggregate by storm NUMBER, not storm name 
                count = mat['count'].groupby(mat['number']).sum().values
                grid[ilat,ilon] = ((count >= 1).astype(np.int32)).sum()
    late_season.append(grid)
late_season = np.array(late_season)

save_3D(os.path.join(dpath, 'BoM_tc_count_%sx%s_%skm_1969_2010_late_season.nc' % tuple(map(int, (res, res, max_dist)))), \
        late_season, 'count', latitudes, longitudes, -999.9, np.arange(late_season.shape[0]))

np.savez('../data/BoM_grid_storm_%sx%s_%skm_1969_2010_late_season' % tuple(map(int, (res, res, max_dist))), late_season=late_season, lat=latitudes, lon=longitudes, season='FMA')
