#!/usr/bin/python
# -*- coding: utf-8 -*-
# ==============================================================================
# code get_all_BOM.py
# Description: download the TC BoM dataset
# TAGS:TC:ICU:BoM
# Nicolas Fauchereau <Nicolas.Fauchereau@gmail.com>
# ==============================================================================

import os, sys
import time
import requests

ystart = int(sys.argv[1])
yend = int(sys.argv[2])

base_url = 'http://www.bom.gov.au/cgi-bin/ws/gis/ncc/tracker/download.cgi?'

f = open("../data/BOM_data_%s-%s.csv" % ( ystart, yend ), "w")

for year in xrange(ystart, yend): 
    for num in xrange(1,100): # we assume no more than 100 TC per season !
        ### ===========================================================================
        ### pause for 5 seconds ... 
        time.sleep(5)
        ### ===========================================================================
        r = requests.get("%syear=%s&seq=%s&type=csv" % (base_url, year, num))
        if r.status_code == 200: 
            lines = r.content.split("\n")
            ### skip the header and ignore the last two empy lines 
            for l in lines[1:-2]:
                f.write("%s\n" % (l))
            r.close()
        else:
            r.close()
            break
f.close()
