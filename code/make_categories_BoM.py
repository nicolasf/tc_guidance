#!/usr/bin/python                                                                                  
# -*- coding: utf-8 -*-
# ==============================================================================
# code make_categories_BoM.py
# Nicolas Fauchereau <Nicolas.Fauchereau@gmail.com>
# ==============================================================================

# ==============================================================================
import os
import numpy as np
import pandas as pd
### ===========================================================================

def return_scale(pressure): 
    """
       returns the scale of a TC given the Minimum central 
       pressure. 
    """ 
    if (pressure >= 986):
        scale = 1
    elif (pressure <= 985) & (pressure >= 971):
        scale = 2
    elif (pressure <= 970) & (pressure >= 956):
        scale = 3
    elif (pressure <= 955) & (pressure >= 930): 
        scale = 4
    elif (pressure <= 929): 
        scale = 5
    else: 
        scale = np.NaN
    return scale


dpath = os.path.join(os.environ['HOME'], 'operational/ICU/TC_guidance/data')

tcs_bom = pd.read_csv(os.path.join(dpath, 'BOM_data_1969-2010.csv'), header=None,\
                                      names=['year','number','name','iso_datetime','lat','lon','central_pressure'], index_col=3, parse_dates=True)

tcs_bom['central_pressure'][tcs_bom['central_pressure'] <= 700] = np.nan

tcs_bom['year'] = tcs_bom.index.year
tcs_bom['month'] = tcs_bom.index.month
tcs_bom['day'] = tcs_bom.index.day

tcs_bom = tcs_bom.ix['1969-1':]

### ===========================================================================
### restrict to 135E 
tcs_bom = tcs_bom[tcs_bom['lon'] >= 135.]

### ===========================================================================
### count the number of TC per year enterring the basin for each season (full, early, late)

for seas in ['full', 'early', 'late']: 
    count = []
    for y in np.arange(1969, 2011):
        if seas == 'full':
            mat = tcs_bom.ix['%s-11' % ( y ):'%s-04' % ( y + 1 )]
        elif seas == 'early':
            mat = tcs_bom.ix['%s-11' % ( y ):'%s-01' % ( y + 1 )]
        elif seas == 'late':
            mat = tcs_bom.ix['%s-02' % ( y ):'%s-04' % ( y )]
        count.append(len(mat.groupby(['number','name']).mean()))
    count = np.array(count)
    count = pd.DataFrame(count, index=np.arange(1969, 2011))
    count.to_csv('../outputs/TCs_BoM_count_%s_season.csv' % ( seas ))


### ===========================================================================
ls_DF = []

for y in np.unique(tcs_bom.year): 
    mat = tcs_bom[tcs_bom.year == y] 
    for name in np.unique(mat.name.values):
        mat2 = mat[mat.name.values==name]
        mat2['cat'] = return_scale(mat2.central_pressure.min())
        ls_DF.append(mat2)

ls_DF = pd.concat(ls_DF)

ls_DF.to_csv(os.path.join(dpath, 'TCs_BoM_1969_2010_135E_wCAT.csv'))
