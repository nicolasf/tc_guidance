#!/usr/bin/python                                                                                  
# ==============================================================================
# code read_SPearTC_csv.py
# Nicolas Fauchereau <Nicolas.Fauchereau@gmail.com>
# ==============================================================================
# -*- coding: utf-8 -*-

### ===========================================================================
### TODO 


import os, sys
import numpy as np
from numpy import ma
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap


opath = os.path.join('/home/nicolasf/operational/ICU/TC_guidance/data/')

### ===========================================================================
### read the npz files 

season_string = 'full'

fname = 'BoM_grid_storm_5x5_500km_1969_2010_%s_season.npz' % ( season_string )

data = np.load(os.path.join(opath, fname))

lat = data['lat']
lon = data['lon']

exec("season = data['%s_season']" % ( season_string))

lons, lats = np.meshgrid(lon, lat)

### ===========================================================================
### ATTENTION index 0 = season 1969 - 70

years_compos = [1978, 1979, 1980, 1981, 1990, 1996, 2001]

if season_string == 'late':
    str_years_compos = ", ".join(map(str, np.array(years_compos) +1 ) )
    compos_years = np.array(years_compos) + 1 - 1970
else: 
    str_years_compos = ", ".join(map(str, years_compos))
    compos_years = np.array(years_compos) - 1969

### ===========================================================================
### calculate composite anomalies: to be consistant the anomalies are calculated WRT 1969/10 - 2009/10 season

season_mean = season.mean(0)

season_compos = np.take(season, compos_years, axis=0).mean(0)

season_anoms = np.take(season, compos_years, axis=0).mean(0) - season[:,:,:].mean(0) 

### ===========================================================================
### interpolates on a higher resolution grid 
from mpl_toolkits.basemap import interp

season_mean = season_mean[::-1,:]
season_compos = season_compos[::-1,:]
season_anoms = season_anoms[::-1,:]

lat = lat[::-1]

latout = np.arange(-60., 0. + .5, .5)
lonout = np.arange(135., 240. + .5, .5)
lonsout, latsout = np.meshgrid(lonout, latout)

season_mean_remapped = interp(season_mean, lon, lat, lonsout, latsout, order=1)
season_compos_remapped = interp(season_compos, lon, lat, lonsout, latsout, order=1)
season_anoms_remapped = interp(season_anoms, lon, lat, lonsout, latsout, order=1)

### ===========================================================================
# create the figure and axes instances
fig = plt.figure(figsize=(8,8))
ax = fig.add_axes([0.1,0.1,0.8,0.8])

cmap = plt.get_cmap('RdBu_r')

### set up the map 
m = Basemap(llcrnrlon=135.,llcrnrlat=-50.,urcrnrlon=240.,urcrnrlat=0,\
            resolution='l',area_thresh=10.,projection='merc',\
            lat_1=50.,lon_0=-107.,ax=ax)
im = m.pcolormesh(lonsout, latsout, season_anoms_remapped, vmin=-1.5, vmax=1.5, latlon=True, cmap=cmap)
#im = m.contourf(lonsout, latsout, season_compos_remapped, np.arange(-1.4, 1.4 + 0.1, 0.1),  latlon=True, cmap=cmap, extend='both')
csn = m.contour(lonsout,latsout, season_anoms_remapped, np.arange(-1.5, 0, 0.2), latlon=True, colors='0.3', linestyles='solid', lineswidths=0.5)
csp = m.contour(lonsout,latsout, season_anoms_remapped, np.arange(0.2, 1.5 + 0.2, 0.2), latlon=True, colors='0.3', linestyles='solid', lineswidths=0.5)
plt.clabel(csn, fmt='%4.1f', colors='0.1')
plt.clabel(csp, fmt='%4.1f', colors='0.1')
m.drawcoastlines(linewidth=0.5, color='0.1')
m.colorbar(im, size='3%', pad='1%', label='number of storms (anomalies)', boundaries=np.arange(-1.5, 1.5 + 0.1, 0.1), drawedges=True)
m.drawmeridians(np.arange(130, 250, 20), labels=[0,0,0,1], color='0.8', linewidth=0)
m.drawparallels(np.arange(-60, 0, 10), labels=[1,0,0,0], color='0.8', linewidth=0)
ax.set_title('TC density anomalies %s season:\n%s' % ( season_string, str_years_compos), fontsize=16)
plt.show()
fig.savefig('../figures/BoM_TC_density_anomalies_%s_season.png' % ( season_string ), dpi=200, bbox_inches='tight')
plt.close(fig)

### ===========================================================================

fig = plt.figure(figsize=(8,8))
ax = fig.add_axes([0.1,0.1,0.8,0.8])

### set up the map 
m = Basemap(llcrnrlon=135.,llcrnrlat=-50.,urcrnrlon=240.,urcrnrlat=0,\
            resolution='l',area_thresh=10.,projection='merc',\
            lat_1=50.,lon_0=-107.,ax=ax)
im = m.pcolormesh(lonsout, latsout, season_compos_remapped, vmin=0.0, vmax=5, latlon=True, cmap=plt.get_cmap('Oranges'))
cs = m.contour(lonsout,latsout, season_compos_remapped, np.arange(0.5, 5, 0.5), latlon=True, colors='k')
plt.clabel(cs, fmt='%4.1f', color='0.5')
m.drawcoastlines(linewidth=0.5, color='0.1')
m.colorbar(im, size='3%', pad='1%', label='average number of storms', boundaries=np.arange(0, 5, 0.5), drawedges=True)
m.drawmeridians(np.arange(130, 250, 20), labels=[0,0,0,1], color='0.8', linewidth=0)
m.drawparallels(np.arange(-60, 0, 10), labels=[1,0,0,0], color='0.8', linewidth=0)
ax.set_title('%s season, years:\n%s' % ( season_string,  str_years_compos), fontsize=16)
fig.savefig('../figures/BoM_TC_density_composite_%s_season.png' % ( season_string ), dpi=200, bbox_inches='tight')
plt.show()
plt.close(fig)

### ===========================================================================

fig = plt.figure(figsize=(8,8))
ax = fig.add_axes([0.1,0.1,0.8,0.8])

### set up the map 
m = Basemap(llcrnrlon=135.,llcrnrlat=-50.,urcrnrlon=240.,urcrnrlat=0,\
            resolution='l',area_thresh=10.,projection='merc',\
            lat_1=50.,lon_0=-107.,ax=ax)
im = m.pcolormesh(lonsout, latsout, season_mean_remapped, vmin=0.0, vmax=5, latlon=True, cmap=plt.get_cmap('Oranges'))
cs = m.contour(lonsout,latsout, season_mean_remapped, np.arange(0.5, 5, 0.5), latlon=True, colors='k')
plt.clabel(cs, fmt='%4.1f', color='0.5')
m.drawcoastlines(linewidth=0.5, color='0.1')
m.colorbar(im, size='3%', pad='1%', label='average number of storms', boundaries=np.arange(0, 5, 0.5), drawedges=True)
m.drawmeridians(np.arange(130, 250, 20), labels=[0,0,0,1], color='0.8', linewidth=0)
m.drawparallels(np.arange(-60, 0, 10), labels=[1,0,0,0], color='0.8', linewidth=0)
ax.set_title('%s season climatology' % ( season_string ), fontsize=16)
fig.savefig('../figures/BoM_TC_density_climatology_%s_season.png' % ( season_string ), dpi=200, bbox_inches='tight')
plt.show()
plt.close(fig)
