#!/usr/bin/python                                                                                  
# -*- coding: utf-8 -*-
# ==============================================================================
# code plot_TC_tracks_BoM.py.py
# Nicolas Fauchereau <Nicolas.Fauchereau@gmail.com>
# ==============================================================================

import os, sys
import numpy as np
from numpy import ma
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap

dpath = os.path.join(os.environ['HOME'], 'operational/ICU/TC_guidance/data')

"""
read TCs from BoM with the Category 
"""
tcs_bom = pd.read_csv(os.path.join(dpath, 'TCs_BoM_1969_2010_135E_wCAT.csv'), parse_dates=True, index_col=0)

"""
restrict to 135E
"""
tcs_bom = tcs_bom[tcs_bom['lon'] >= 135.]

"""
composite years
"""
compos_years = [1978, 1979, 1980, 1981, 1990, 1996, 2001]

for y in compos_years: 

    """
    Select November to April for each year 
    """
    mat = tcs_bom.ix['%s-11' % ( y ):'%s-04' % ( y + 1 )]

    """
    Counts the number of storms in each categorie 
    """

    cats = pd.Series(mat.groupby(['number','name'])['cat'].min())
    catn = dict(cats.value_counts())
    ucat = cats.isnull().sum()
    for i in xrange(1,6): 
        if catn.get(i) is None:
            catn[i] = 0
    catn = str(catn).strip("{").strip("}").replace(":", " =")

    storms_unique = np.unique(mat.number)
    names_unique  = np.unique(mat.name)

    fig = plt.figure(figsize=(10,10))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
    # setup of basemap ('lcc' = lambert conformal conic).
    # use major and minor sphere radii from WGS84 ellipsoid.
    m = Basemap(llcrnrlon=135.,llcrnrlat=-60.,urcrnrlon=240.,urcrnrlat=0, resolution='h',area_thresh=10.,projection='cyl', lat_1=-10.,lon_0=180.,ax=ax)

    ### ===========================================================================
    ### loop over each unique storm (hopefully) and plot the corresponding track along with the wind-speed information
    for idstorm in storms_unique: 
        storm = mat.ix[mat['number'] == idstorm]
        #if 'UNAMED' not in np.unique(storm.name.values)[0]:
        lat_track = storm.lat.values
        lon_track = storm.lon.values 
        ### shift the lons to go from 0 to 360, NOT from -180 to 180
        lon_track[lon_track < 0] += 360.
        ### save the corrected longitude vector into the dataframe
        storm['lonmod'] = lon_track
        ### concatenate the dataframes along the time axis
        ### ===========================================================================
        ### now plot 
        ### coordinates transforms
        xx,yy = m(lon_track, lat_track)
        m.plot(xx, yy,linewidth=2, zorder=12)
        c = m.scatter(xx,yy,s=50,c=storm['central_pressure'], cmap=plt.get_cmap('jet_r'), edgecolor = 'none', marker='o', vmin=920, vmax=1010)

    ax.set_color_cycle(['c', 'm', 'y', 'k'])

    m.colorbar(c, label='central pressure (hPa)', size='3%',pad='0.001%', boundaries=np.arange(920, 1010, 10), drawedges=True)
    m.drawcoastlines(linewidth=1, color='0.3', zorder=12)
    m.drawmeridians(np.arange(100, 260, 20), labels=[0,0,0,1], color='k', linewidth=0.5)
    m.drawparallels(np.arange(-60, 0, 10), labels=[1,0,0,0], color='k', linewidth=0.5)

    ax.set_title('Full Season Plot for season %s-%s\n%s storms, cat.: %s' % (y, y + 1, len(storms_unique), catn), fontsize=16)

    plt.savefig('../figures/BoM_Full_Season_Plot_year_%s.png' % ( y ), dpi=200, bbox_inches='tight')

### EOF 
